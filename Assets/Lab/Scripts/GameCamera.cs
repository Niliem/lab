﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour 
{
    public static GameCamera Instance;

    private float _maxTime = 1.0f;
    private float _curTime;

    private float _speed = 15.0f;

    private int rot = 0;
    private float curAngle = 0.0f;

	void Awake () 
    {
        _curTime = _maxTime;
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of RotateCamera!");
        }
        Instance = this;
	}

    public void RotateCamera()
    {
        transform.Rotate(0.0f, 0.0f, Time.deltaTime);
        if (camera.orthographicSize > 0.5f)
        {
            camera.orthographicSize -= 0.0005f;
        }
    }

    public void RandomRotateCamera()
    {
        camera.orthographicSize = 3;
        if (_curTime > 0.0f)
        {
            _curTime -= Time.deltaTime;
        }
        else if (_curTime <= 0.0f)
        {
            rot = Random.Range(0, 5);
            curAngle = rot * 90.0f;
            _curTime = _maxTime;
        }
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0.0f, 0.0f, curAngle)), _speed * Time.deltaTime);
    }
}
