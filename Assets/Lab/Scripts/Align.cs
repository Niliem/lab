﻿using UnityEngine;
using System.Collections;

public class Align : MonoBehaviour 
{
    public Transform target;

    [System.Serializable]
    public class Position
    {
        public bool X = false;
        public bool Y = false;
        public bool Z = false;
    }
    public Position position = new Position();

	void Update () 
    {
        if (position.X && target)
            transform.position = new Vector3(target.position.x, transform.position.y, transform.position.z);
        if (position.Y && target)
            transform.position = new Vector3(transform.position.x, target.position.y, transform.position.z);
        if (position.Z && target)
            transform.position = new Vector3(transform.position.x, transform.position.y, target.position.z);
	}
}
