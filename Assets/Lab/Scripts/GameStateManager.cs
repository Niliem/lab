﻿using UnityEngine;
using System.Collections;

public class GameStateManager : MonoBehaviour 
{
    public static GameStateManager Instance;

    public GameObject player;
    public GameObject level;

    public GameObject[] panels;

    public GameCoreState _state;
    public GameCoreState state
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
        }
    }

    public GameState _stateGame;
    public GameState stateGame
    {
        get
        {
            return _stateGame;
        }
        set
        {
            _stateGame = value;
        }
    }

    void Awake()
    {
        state = GameCoreState.STATE_MENU;
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of GameStateManager!");
        }
        Instance = this;
    }

    void Update()
    {
        if (state == GameCoreState.STATE_MENU)
        {
            stateGame = GameState.STATE_GAME_NONE;
            SetState("PanelMenu");
            player.SetActive(false);
            level.SetActive(false);
        }
        if (state == GameCoreState.STATE_OPTIONS)
        {
            stateGame = GameState.STATE_GAME_NONE;
            SetState("PanelOptions");
            player.SetActive(false);
            level.SetActive(false);
        }
        if (state == GameCoreState.STATE_GAME)
        {
            player.SetActive(true);
            level.SetActive(true);
            GameCamera.Instance.camera.backgroundColor = ColorManager.Instance.GetColor(PlayerMove.Instance.Direction);
            if (stateGame == GameState.STATE_GAME_PLAY)
            {
                SetState("PanelGamePlay");
                GameCamera.Instance.RandomRotateCamera();
            }
            if (stateGame == GameState.STATE_GAME_PAUSE)
            {
                SetState("PanelGamePause");
            }
            if (stateGame == GameState.STATE_GAME_DEAD)
            {
                SetState("PanelGameDead");
                GameCamera.Instance.RotateCamera();
            }
        }        
    }

    public void SetState(string tagName)
    {
        foreach(GameObject p in panels)
        {
            if (p.tag == tagName)
            {
                p.SetActive(true);
                continue;
            }
            p.SetActive(false);
        }
    }
}
