﻿using UnityEngine;
using System.Collections;

public class ArrowManager : MonoBehaviour
{
    public static ArrowManager Instance;

    private GameObject[] _arrows;
    public GameObject[] Arrows
    {
        get 
        { 
            return _arrows; 
        }
        set 
        { 
            _arrows = value; 
        }
    }
    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of ArrowManager!");
        }
        Instance = this;
    }


    void Start()
    {
        Arrows = GameObject.FindGameObjectsWithTag("Arrow");
    }

    public void ClearArrow(GameObject arrow)
    {
        foreach(GameObject a in Arrows)
        {
            if (!Equals(a, arrow))
            {
                a.GetComponent<ArrowDirection>().isCan = false;
            }
        }
    }
}
