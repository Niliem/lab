﻿using UnityEngine;
using System.Collections;

public class ColorManager : MonoBehaviour 
{
    public static ColorManager Instance;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of ColorManager!");
        }
        Instance = this;
    }

    public Color32 UpColor = new Color32(255, 255, 0, 200);
    public Color32 DownColor = new Color32(255, 0, 255, 200);
    public Color32 LeftColor = new Color32(0, 255, 255, 200);
    public Color32 RightColor = new Color32(0, 255, 0, 200);

    public Color GetColor(MoveDirection direction)
    {
        switch (direction)
        {
            case MoveDirection.Up:
                return UpColor;
            case MoveDirection.Down:
                return DownColor;
            case MoveDirection.Left:
                return LeftColor;
            case MoveDirection.Right:
                return RightColor;
        }
        return GetColor(GlobalVariables.NextDirection);
    }

    [ContextMenu("ChangeColor")]
    private void Change()
    {
        GameObject[] go = GameObject.FindGameObjectsWithTag("Arrow");
        foreach (GameObject g in go)
        {
            g.GetComponent<SpriteRenderer>().color = GetColor(g.GetComponent<ArrowDirection>().direction);
        }
        GameObject cam = GameObject.FindGameObjectWithTag("MainCamera");
        cam.camera.backgroundColor = Color.black;
    }
}
