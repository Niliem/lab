﻿using UnityEngine;
using System.Collections;

public class InputGameManager : MonoBehaviour 
{
    public static InputGameManager Instance;

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of InputGameManager!");
        }
        Instance = this;
    }

    public void MovePlayer()
    {
        if (GameStateManager.Instance.stateGame == GameState.STATE_GAME_PLAY)
        {
            PlayerMove.Instance.ChangeDirection(GlobalVariables.NextDirection);
        }
    }

    public void Restart()
    {
        PlayerMove.Instance.isDead = false;
        GameStateManager.Instance.player.transform.position = new Vector3(GlobalVariables.StartX, GlobalVariables.StartY, 0);
        PlayerMove.Instance.ChangeDirection(GlobalVariables.StartDirection);
        Resume();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        GameStateManager.Instance.stateGame = GameState.STATE_GAME_PAUSE;
    }

    public void Resume()
    {
        Time.timeScale = 1;
        GameStateManager.Instance.stateGame = GameState.STATE_GAME_PLAY;
    }
    
    public void Play()
    {
        GameStateManager.Instance.state = GameCoreState.STATE_GAME;
        GameStateManager.Instance.stateGame = GameState.STATE_GAME_PLAY;        
        InputGameManager.Instance.Restart();        
    }
}
