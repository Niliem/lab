﻿using UnityEngine;
using System.Collections;

public class ArrowDirection : MonoBehaviour 
{
    public MoveDirection direction;
    public bool isCan = false;

    void Start()
    {
        gameObject.GetComponent<SpriteRenderer>().color = ColorManager.Instance.GetColor(direction);
    }

    void Update()
    {
        if (PlayerMove.Instance.Direction != direction)
        {
            if(isCan)
            {
                GlobalVariables.NextDirection = direction;
            }            
        }
        else
        {
            isCan = false;           
        }
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.tag == "Player")
        {
            isCan = true;
            ArrowManager.Instance.ClearArrow(gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D obj)
    {
        if (obj.tag == "Player")
        {
            isCan = false;
        }
    }
}
