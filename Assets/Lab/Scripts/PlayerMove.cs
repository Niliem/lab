﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{
    public static PlayerMove Instance;

    public float speed = 2.0f;

    public bool isDead = false;    

    private MoveDirection _prevDirection;
    private MoveDirection _direction;

    public MoveDirection Direction
    {
        get
        {
            return _direction;
        }
        set
        {
            _direction = value;
        }
    }

    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Multiple instances of PlayerMove!");
        }
        Instance = this;

        GlobalVariables.StartX = transform.position.x;
        GlobalVariables.StartY = transform.position.y;

        Direction = MoveDirection.None;
        GlobalVariables.StartDirection = Direction;
    }

    void Update()
    {
        switch (Direction)
        {
            case MoveDirection.Up:
                rigidbody2D.velocity = new Vector2(0.0f, speed);
                break;
            case MoveDirection.Down:
                rigidbody2D.velocity = new Vector2(0.0f, -speed);
                break;
            case MoveDirection.Left:
                rigidbody2D.velocity = new Vector2(-speed, 0.0f);
                break;
            case MoveDirection.Right:
                rigidbody2D.velocity = new Vector2(speed, 0.0f);
                break;
            case MoveDirection.None:
            default:
                rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
                break;
        }
    }

    void OnCollisionEnter2D(Collision2D obj)
    {
        if (obj.gameObject.tag == "Wall")
        {
            GameStateManager.Instance.stateGame = GameState.STATE_GAME_DEAD;
            rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
            isDead = true;
        }
    }

    public void ChangeDirection(MoveDirection direction)
    {
        _prevDirection = Direction;
        Direction = direction;
    }

    private void ReturnDirection()
    {
        Direction = _prevDirection;
    }
}
