﻿public enum GameCoreState
{
    STATE_MENU,
    STATE_OPTIONS,
    STATE_GAME   
}

public enum GameState
{
    STATE_GAME_PLAY,
    STATE_GAME_PAUSE,
    STATE_GAME_DEAD,
    STATE_GAME_NONE
}
